//
//  CartVC.swift
//  Ecom DXMinds
//
//  Created by admin on 09/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit
import SDWebImage
class CartVC: UITableViewController, AlertDelegate
{
    func performAction() {
        
    }
    
    let shared = EcomSingleton.shared
    struct Storyboard {
        static let numberOfItemsCell = "numberOfItemsCell"      // cell 0
        static let itemCell = "itemCell"                        // cell 1
        static let cartDetailCell = "cartDetailCell"            // cell 2
        static let cartTotalCell = "cartTotalCell"              // cell 3
        static let checkoutButtonCell = "checkoutButtonCell"    // cell 4
    }
    var cart:CartModel?
    var products: [ProductListModel]?
    
    override func viewWillAppear(_ animated: Bool) {
        products = shared.cart
        cart = CartModel.init(products: products ?? [])
        self.tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
    }
}

extension CartVC
{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let products = products {
            return products.count + 4
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let products = products else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.numberOfItemsCell, for: indexPath) as! NumberOfItemsCell
            cell.numberOfItemsLabel.text = "\(0) ITEM"
            
            return cell
        }
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.numberOfItemsCell, for: indexPath) as! NumberOfItemsCell
            cell.numberOfItemsLabel.text = "\(products.count) ITEMS"
            return cell
        } else if indexPath.row == products.count + 1 {
            // cartDetailCell
            let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.cartDetailCell, for: indexPath) as! CartBillCell
            cell.cart = cart
            
            return cell
        } else if indexPath.row == products.count + 2 {
            // cartTotalCell
            let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.cartTotalCell, for: indexPath) as! CartTotalCell
            cell.cart = cart
            
            
            return cell
        } else if indexPath.row == products.count + 3 {
            // checkoutButtonCell
            let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.checkoutButtonCell, for: indexPath)
             as? CartCheckOutCell
            cell?.checkOut.addTarget(self, action: #selector(toCheckOut), for: .touchUpInside)
            return cell!
        } else {
            // itemCell
            let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.itemCell, for: indexPath) as! ShoppingCartItemCell
            cell.product = products[indexPath.row - 1]
            cell.reloadDataDelegate = self
            return cell
        }
    }
    
    @objc func toCheckOut() {
        EcomSingleton.shared.showAlertMessage(title: "Thank you", message: "Checkout will be done later dint get time!", delegate: self, controller: self)
    }
    
}

extension CartVC:CartDelegate {
    func reloadData() {
            products = shared.cart
                   cart = CartModel.init(products: products ?? [])
                   self.tableView.reloadData()
            self.tableView.reloadData()
        
        
    }
    
    
}
class NumberOfItemsCell: UITableViewCell
{
    @IBOutlet weak var numberOfItemsLabel: UILabel!
}

class ShoppingCartItemCell : UITableViewCell
{
    weak var reloadDataDelegate:CartDelegate!
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    
    var product: ProductListModel! {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI()
    {
        productImageView.sd_setImage(with: URL(string: (product.imageUrl.first ?? "")!))
        productNameLabel.text =  product.title
        productPriceLabel.text = "₹\(product.price)"
    }
    
    @IBAction func removeButtonDidTap(_ sender: Any) {
        EcomSingleton.shared.cartId.remove(product.id)
        if let index = find(value: product.id, in: EcomSingleton.shared.cart) {
        EcomSingleton.shared.cart.remove(at:index)
            reloadDataDelegate.reloadData()
            
        }
    }
    
    func find(value searchValue: Int, in array: [ProductListModel]) -> Int?
    {
        for (index, value) in array.enumerated()
        {
            if value.id == searchValue {
                return index
            }
        }

        return nil
    }
    
}


class CartBillCell: UITableViewCell {
    @IBOutlet weak var subTotalLable: UILabel!
    
    @IBOutlet weak var taxLabel: UILabel!
    var cart: CartModel! {
        didSet {
            self.updateUI()
        }
    }
    func updateUI()
    {
        
        subTotalLable.text =  "₹\(cart.subTotlal)"
    }
    
}

class CartTotalCell: UITableViewCell {
    
    @IBOutlet weak var totalLabel: UILabel!
    var cart: CartModel! {
        didSet {
            self.updateUI()
        }
    }
    func updateUI()
    {
        
        totalLabel.text =  "₹\(cart.total)"
    }
}

class CartCheckOutCell: UITableViewCell {
    
    @IBOutlet weak var checkOut: UIButton!
    
}

extension Array where Element: Equatable {
    func indexes(of element: Element) -> [Int] {
        return self.enumerated().filter({ element == $0.element }).map({ $0.offset })
    }
}
