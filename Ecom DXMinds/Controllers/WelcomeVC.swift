//
//  WelcomeVC.swift
//  Ecom DXMinds
//
//  Created by admin on 08/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    enum SegueIds {
        static let register = "ToRegisterVC"
        static let login = "ToLoginVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerClicked(_ sender: Any) {
        
            performSegue(withIdentifier: SegueIds.register, sender: nil)
        
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        
        performSegue(withIdentifier: SegueIds.login, sender: nil)
    }
    
}





























