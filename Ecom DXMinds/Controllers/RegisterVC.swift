//
//  RegisterVC.swift
//  Ecom DXMinds
//
//  Created by admin on 08/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit
import Firebase
class RegisterVC: UIViewController, AlertDelegate {
    func performAction() {
        
    }
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    let keyWindow = UIApplication.shared.connectedScenes
    .filter({$0.activationState == .foregroundActive})
    .map({$0 as? UIWindowScene})
    .compactMap({$0})
    .first?.windows
    .filter({$0.isKeyWindow}).first
    enum Segues {
        static let home = "ToHome"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func registerUser(_ sender: Any) {
        LoadingOverlay.shared.showOverlay(view: keyWindow!)
        if let email = emailTxt.text, let password = passwordTxt.text {
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                if let error = error {
                    print(error.localizedDescription)
                     EcomSingleton.shared.showAlertMessage(title: "Error", message: error.localizedDescription, delegate: self,controller:self)
                } else {
                    LoadingOverlay.shared.showOverlay(view: self.keyWindow!)
                    self.performSegue(withIdentifier: Segues.home, sender: nil)
                }
                
            }
        }
    }
    
    
    
    
}
