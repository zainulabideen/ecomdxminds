//
//  HomeVC.swift
//  Ecom DXMinds
//
//  Created by admin on 08/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SDWebImage


class HomeVC: UIViewController {
    
    var index:Int?
    let ecomSharedInstance = EcomSingleton.shared
    
    
    enum Identifiers {
        static let listCell = "ProductListCell"
    }
    
    enum Segues {
        static let detail = "ToDetailsVC"
    }
    var products = [ProductListModel]()
    @IBOutlet weak var productListView: UITableView!
    
    
    
    lazy var cartButton:UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.appColor(.themeColor)
        button.setTitle("Cart", for: .normal)
        return button
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Dash Board"
        self.navigationController?.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cart", style: .plain, target: nil, action: nil)
        configureListView()
        ecomSharedInstance.configureFirebaseDB()
        getProducts()
        
    }
    
    
    fileprivate func configureListView() -> (Void) {
        
        productListView.dataSource = self
        productListView.delegate = self
        
        let nib = UINib(nibName: Identifiers.listCell, bundle: nil)
        productListView.register(nib, forCellReuseIdentifier: Identifiers.listCell)
    }
    
    fileprivate func getProducts() {
        ecomSharedInstance.getProducts(collectionName: "products") { (dataSnap) in
            for prod in dataSnap {
                if let p = prod.value as? [String:Any] {
                    let product = ProductListModel(id: p["id"] as! Int, Description: p["Description"] as! String, imageUrl: p["imageUrl"] as! [String], price: p["price"] as! Double, title: p["title"] as! String, detail: "")
                    self.products.append(product)
                }
                self.ecomSharedInstance.products = self.products
            }
            self.productListView.reloadData()
        }
        
    }
    
    
}

//Mark Table Delegate
extension HomeVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:Identifiers.listCell) as? ProductListCell
        let product = products[indexPath.item]
        cell?.prodIV?.sd_setImage(with: URL(string: product.imageUrl[0] ?? ""))
        cell?.titleLbl.text = product.title
        cell?.descriptionLbl.text = product.Description
        cell?.priceLbl.text = "₹\(product.price)"
        
        return cell!
    }

    
}

extension HomeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.index = indexPath.item
        performSegue(withIdentifier: Segues.detail, sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segues.detail {
            let destination = segue.destination as? ProductDetailsVC
            destination?.index = self.index
            
        }
    }
}

    
    

enum AssetsColor {
   case themeColor
   case themeCresent
}

extension UIColor {

    static func appColor(_ name: AssetsColor) -> UIColor? {
        switch name {
        case .themeColor:
            return UIColor(named: "themeColor")
        case .themeCresent:
            return UIColor(named: "themeCresentColor")
        }
    }
}
