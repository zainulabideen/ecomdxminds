//
//  ProductDetailsVC.swift
//  Ecom DXMinds
//
//  Created by admin on 08/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit

class ProductDetailsVC: UIViewController, AlertDelegate {
    func performAction() {
        performSegue(withIdentifier: Segues.cart, sender: nil)
    }
    

    weak var imageDelegate:ImageDelegate!
    enum Identifiers {
        static let prodDetailsCell = "ProductDetailsScrollCell"
    }
    enum Segues {
        static let cart = "ToCart"
    }
    var index : Int?
    @IBOutlet weak var proDetailsView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        proDetailsView.dataSource = self
        proDetailsView.delegate = self
        proDetailsView.estimatedRowHeight = 400
        proDetailsView.rowHeight = UITableView.automaticDimension
        proDetailsView.register(UINib(nibName: Identifiers.prodDetailsCell, bundle: nil), forCellReuseIdentifier: Identifiers.prodDetailsCell)

        // Do any additional setup after loading the view.
    }

}

extension ProductDetailsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.prodDetailsCell) as? ProductDetailsScrollCell
        let product = EcomSingleton.shared.products[index ?? 0]
        cell?.titleLbl.text = product.title
        cell?.descriptionLbl.text = product.Description
        cell?.priceLbl.text = "₹\(product.price)"
        self.imageDelegate = cell
        self.imageDelegate.getImages(stringUrls: product.imageUrl)
        cell?.addToCartBtn.addTarget(self, action: #selector(addToCart), for: .touchUpInside)
        return cell!
    }
    
    @objc func addToCart() {
//        save locally
          let res =   EcomSingleton.shared.cartId.insert(EcomSingleton.shared.products[index ?? 0].id)
        if res.inserted {
            print("Added to cart")
            EcomSingleton.shared.cart.append(EcomSingleton.shared.products[index ?? 0])
            EcomSingleton.shared.showAlertMessage(title: "Success", message: "Added to cart", delegate: self,controller:self)
            
        } else {
            print("Already in cart")
            EcomSingleton.shared.showAlertMessage(title: "Alert", message: "Already in cart", delegate: self,controller:self)
        }
    }
    
}

extension ProductDetailsVC : UITableViewDelegate {
    
}


