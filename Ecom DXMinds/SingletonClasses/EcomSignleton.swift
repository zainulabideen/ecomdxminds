//
//  EcomSignleton.swift
//  Ecom DXMinds
//
//  Created by admin on 09/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import Foundation
import Firebase
protocol AlertDelegate:class {
    func performAction()
}
class EcomSingleton {
    
    static let shared = EcomSingleton()
    var ref: DatabaseReference!
    public var products : [ProductListModel] = []
    public var cartId:Set = Set<Int>()
    public var cart : [ProductListModel] = []
    private init () {
        
    }
    
     func configureFirebaseDB() -> (Void) {
        
        ref = Database.database().reference()
    }
    
    public func getProducts(collectionName:String,completion: @escaping (_ dataSnap: [DataSnapshot]) -> Void) {
        self.ref.child(collectionName).queryOrderedByKey().observe(.value) { (snapData) in
            if let data = snapData.children.allObjects as? [DataSnapshot] {
                completion(data)
            }
        }
    }
    
    public func showAlertMessage(title:String, message:String,delegate:AlertDelegate,controller:UIViewController)->Void{
        
        weak var alertDelegate:AlertDelegate!
        
        alertDelegate = delegate
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            alertDelegate.performAction()
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    
     
}
