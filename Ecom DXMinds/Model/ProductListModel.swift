//
//  ProductListModel.swift
//  Ecom DXMinds
//
//  Created by admin on 08/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit
import Foundation
struct ProductListModel: Codable {

    let id : Int
    let Description : String?
    let imageUrl : [String?]
    let price : Double
    let title : String?
    let detail : String?
    
    init(id:Int,Description:String,imageUrl:[String],price:Double,title:String,detail:String) {
        self.id = id
        self.Description = Description
        self.imageUrl = imageUrl
        self.price = price
        self.title = title
        self.detail = detail
    }
}
