//
//  CartModel.swift
//  Ecom DXMinds
//
//  Created by admin on 09/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import Foundation

struct CartModel {
    var products:[ProductListModel] = []
    var subTotlal:Double {
       self.products.reduce(0, {$0 + $1.price})
    }
    var total:Double {
        subTotlal+12
    }
    
    
}
