//
//  ProductDetailsScrollCell.swift
//  Ecom DXMinds
//
//  Created by admin on 08/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit
import SDWebImage
protocol CartDelegate:class {
    func reloadData()
}
protocol ImageDelegate:class {
    func getImages(stringUrls: [String?])
}
class ProductDetailsScrollCell: UITableViewCell {
    var delegate: CartDelegate?
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var imageHorizontalList: UICollectionView!
    
    var productImages : [String?] = []
    
    enum Identifiers {
        static let imagecell = "ProductImageCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageHorizontalList.dataSource = self
        imageHorizontalList.delegate = self
        imageHorizontalList.isPagingEnabled = true
        imageHorizontalList.register(UINib(nibName: Identifiers.imagecell, bundle: nil), forCellWithReuseIdentifier: Identifiers.imagecell)
    }

    @IBAction func addToCart(_ sender: Any) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}

extension ProductDetailsScrollCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.imagecell, for: indexPath) as? ProductImageCell
        cell?.indexLbl.text = "\(indexPath.item+1)/\(productImages.count)"
        cell?.prodImage.sd_setImage(with: URL(string: productImages[indexPath.item] ?? ""))
        return cell!
    }
    
    
}

extension ProductDetailsScrollCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: self.imageHorizontalList.frame.width, height: self.imageHorizontalList.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension ProductDetailsScrollCell: ImageDelegate {
    func getImages(stringUrls: [String?]) {
        self.productImages = stringUrls
        DispatchQueue.main.async {
            self.imageHorizontalList.reloadData()
        }
    }
    
    
    
    
}
