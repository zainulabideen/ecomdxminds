//
//  ProductImageCell.swift
//  Ecom DXMinds
//
//  Created by admin on 08/07/20.
//  Copyright © 2020 Zain. All rights reserved.
//

import UIKit

class ProductImageCell: UICollectionViewCell {

    @IBOutlet weak var indexLbl: UILabel!
    @IBOutlet weak var prodImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
